<?php

namespace Drupal\leaf_group\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks if a group author has only one group personal workspace.
 *
 * @Constraint(
 *   id = "GroupPersonalWorkspaceOwner",
 *   label = @Translation("Group personal workspace owner", context = "Validation"),
 * )
 */
class GroupPersonalWorkspaceOwnerUnique extends Constraint {

  public $message = 'A user can only have one personal workspace.';

}
