<?php

namespace Drupal\leaf_group\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates that the author of the group of type personal workspace has only
 * one.
 */
class GroupPersonalWorkspaceOwnerUniqueValidator extends ConstraintValidator {

  /**
   * @inheritDoc
   */
  public function validate($items, Constraint $constraint) {
    $field_name = $items->getFieldDefinition()->getName();
    if (!($item = $items->first()) || $field_name !== 'uid') {
      return;
    }

    /** @var \Drupal\Core\Entity\EntityInterface $entity */
    $entity = $items->getEntity();
    $id_key = $entity->getEntityType()->getKey('id');
    $entity_type_id = $entity->getEntityTypeId();
    if ($entity_type_id !== 'group' || $entity->bundle() !== 'personal_workspace') {
      return;
    }

    $query = \Drupal::entityQuery($entity_type_id);
    $query->accessCheck(FALSE);

    $entity_id = $entity->id();
    if (isset($entity_id)) {
      $query->condition($id_key, $entity_id, '<>');
    }

    $user_has_workspace = (bool) $query->condition($field_name, $item->value)
      ->range(0, 1)
      ->count()
      ->execute();
    if ($user_has_workspace) {
      // @todo pass the user display name as context.
      $this->context->addViolation($constraint->message, [
        '%value' => $item->value,
        '@entity_type' => $entity->getEntityType()->getSingularLabel(),
        '@field_name' => mb_strtolower($items->getFieldDefinition()->getLabel()),
      ]);
    }
  }

}
